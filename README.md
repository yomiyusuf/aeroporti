# Aeroporti

Android app showing list of airports.

## Running

`./gradlew test` to run the unit tests.
`./gradlew installDebug` to install to a connected device/emulator (minSdk 21)

## Architecture

I opted for the MVVM architecture. I prefer this over, say MVP, because it encourages proper separation of concerns and also enforces the observer pattern.
This can be better for testing.
I also used Hilt for DI, whic is my first time. Just wanted to learn something new.

MVVM helps with state retention across configuration changes, as the ViewModel lifecycle is not connected to the View itself. Allowing the view to be destroyed and recreated without state loss easily.

I opted for a 'Single-activity-multiple-fragment' structure for this app. This is generally recommended by Google for similar apps.
I also used Jetpack navigation for navigating to destinations.


## Improvements
* Very glaringly, the /airport endpoint does not support paging. This means we have to retrieve a long list of airports at once. By using the recyclerview, the UI performance does not suffer, but this could be waste of network resources. Implementing pagination on the endpoint would be advised.
* I didn't have the time to implement a custom layout for larger screens. I could use the screen realeastate better by implementing a grid layout for tablets
* Possibly, also use a master-detail layout fot tablets.
* Implement sorting for the Airports list.
* Implement search (technically, filteration) for the Airports list.
* Display mapview in details screen with airport location marker.
* Write tests for DetailsViewmodel similar to HomeFragment