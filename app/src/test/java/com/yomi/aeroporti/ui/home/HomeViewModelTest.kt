package com.yomi.aeroporti.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.yomi.aeroporti.R
import com.yomi.aeroporti.TestObserver
import com.yomi.aeroporti.common.UiEvent
import com.yomi.aeroporti.data.AirportsRepository
import com.yomi.aeroporti.data.Result
import com.yomi.aeroporti.domain.Airport
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class HomeViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var repository: AirportsRepository
    private lateinit var testCoroutineDispatcher: TestCoroutineDispatcher
    private lateinit var underTest: HomeViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        testCoroutineDispatcher = TestCoroutineDispatcher()

        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @Test
    fun `when viewModel is initialized, data is loaded and Success state emitted`() = runBlockingTest {
        initializeViewmodel()
        val stateObserver = TestObserver<HomeUiState>()
        underTest.state.observeForever(stateObserver)
        assertThat(
            stateObserver.values, equalTo(
                listOf(
                    HomeUiState.Success(listOf())
                )
            )
        )
    }

    @Test
    fun `when repository returns error, error state is emitted`() = runBlockingTest {
        initializeViewmodelWithError()
        val stateObserver = TestObserver<HomeUiState>()
        underTest.state.observeForever(stateObserver)
        assertThat(
            stateObserver.values, equalTo(
                listOf(
                    HomeUiState.Error(R.string.generic_error_message)
                )
            )
        )
    }

    @Test
    fun `when a list item is clicked, user is navigated to details fragment`() {
        initializeViewmodel()
        val eventObserver = TestObserver<UiEvent<HomeUiEvents>>()
        underTest.events.observeForever(eventObserver)
        underTest.onAirportClicked(AIRPORT)
        assertThat(
            eventObserver.values[0].peekContent(), equalTo((HomeUiEvents.NavigateToDetail(AIRPORT)))
        )
    }

    private fun initializeViewmodel() {
        val expectedResult = listOf<Airport>()
        coEvery { repository.getAllAirports() } returns Result.Ok(expectedResult)
        underTest = HomeViewModel(repository, testCoroutineDispatcher)
    }

    private fun initializeViewmodelWithError() {
        val exception = UnknownHostException("exception")
        coEvery { repository.getAllAirports() } returns Result.Error(exception)
        underTest = HomeViewModel(repository, testCoroutineDispatcher)
    }
}

val AIRPORT = HomeAirportUiModel(
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    isInternational = true,
    isDomestic = true
)