package com.yomi.aeroporti.data

import com.yomi.aeroporti.api.AirportsService
import com.yomi.aeroporti.api.model.AirportApiModel
import com.yomi.aeroporti.domain.toAirport
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class AirportsRepositoryTest {

    @MockK
    private lateinit var airportsService: AirportsService
    private lateinit var underTest: AirportsRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        underTest = AirportsRepository(airportsService)
    }

    @Test
    fun `when api request is successful, getAllAirports bubbles up Success result`() = runBlockingTest {
        // Arrange
        val result: List<AirportApiModel> = listOf()
        coEvery { airportsService.getAirports() } returns result

        // Action
        val response = underTest.getAllAirports()
        val list = result.map { it.toAirport() }

        // Assert
        assertThat(response, IsEqual.equalTo(Result.Ok(list)))
    }

    @Test
    fun `when api request fails, getAllAirports bubbles up Error result`() = runBlockingTest {
        // Arrange
        val exception = UnknownHostException("exception")
        coEvery { airportsService.getAirports() } throws  exception

        // Action
        val response = underTest.getAllAirports()

        // Assert
        assertThat(response, IsEqual.equalTo(Result.Error(exception)))
    }
}