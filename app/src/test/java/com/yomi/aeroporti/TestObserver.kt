package com.yomi.aeroporti

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer

class TestObserver<T>(private val handler: (TestObserver<T>.(T) -> Unit) = {}) : Observer<T>, LifecycleOwner {
    private val lifecycle = LifecycleRegistry(this)
    private val _values = mutableListOf<T>()
    val values: List<T>
        get() = _values.toList()

    init {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun getLifecycle(): Lifecycle = lifecycle

    override fun onChanged(t: T) {
        this.handler(t)
        _values.add(t)
    }

    fun destroy() {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }
}
