package com.yomi.aeroporti.domain

import com.yomi.aeroporti.api.model.AirportApiModel
import com.yomi.aeroporti.api.model.City
import com.yomi.aeroporti.api.model.Country
import com.yomi.aeroporti.api.model.Location
import org.junit.Assert.assertEquals
import org.junit.Test

class MapperTest {
    @Test
    fun `AirportApiModel is correctly mapped to Airport object`() {
        // Arrange
        val expected = Airport(
            code = "CODE",
            name = "name",
            cityCode = "cityCode",
            city = "cityName",
            country = "countryName",
            timezone = "timezone",
            long = 10.0,
            lat = 10.0,
            isInternational = true,
            isDomestic = true
        )

        // Action
        val airport = airportApiModelTemplate.toAirport()

        // Assert
        assertEquals(expected, airport)
    }

    @Test
    fun `Null is returned when airportName is null`() {
        // Arrange

        // Action
        val airport = airportApiModelTemplate.copy(airportName = null).toAirport()

        // Assert
        assertEquals(null, airport)
    }

    @Test
    fun `Null is returned when countryName is null`() {
        // Arrange

        // Action
        val airport = airportApiModelTemplate.copy(country = null).toAirport()

        // Assert
        assertEquals(null, airport)
    }
}

val airportApiModelTemplate = AirportApiModel(
    airportCode = "CODE",
    internationalAirport = true,
    domesticAirport = true,
    airportName = "name",
    city = City(cityCode = "cityCode", cityName = "cityName", timeZoneName = "timezone"),
    country = Country(countryCode = "countryCode", countryName = "countryName"),
    location = Location(longitude = 10.0, latitude = 10.0)
)
