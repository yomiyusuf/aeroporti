package com.yomi.aeroporti.data

/**
 * A sealed class representing a result from a call
 */
sealed class Result<out T> {
    /**
     * The call was successful
     */
    data class Ok<out T>(val value: T) : Result<T>()

    /**
     * The call failed due to the specified exception
     */
    data class Error(val cause: Exception) : Result<Nothing>()
}

/**
 * Calls the provided functions, depending on the result
 */
inline fun <T, V> Result<V>.map(failure: (Exception) -> T, success: (V) -> T): T {
    return when (this) {
        is Result.Ok -> success(this.value)
        is Result.Error -> failure(this.cause)
    }
}


inline fun <T> wrap(fn: () -> T) = try {
    Result.Ok(fn())
} catch (e: Exception) { //We could also catch specific exceptions (such as IOExceptions) here
    Result.Error(e)
}
