package com.yomi.aeroporti.data

import com.yomi.aeroporti.api.AirportsService
import com.yomi.aeroporti.domain.toAirport
import javax.inject.Inject

class AirportsRepository @Inject constructor(
    private val airportsService: AirportsService
) {
    suspend fun getAllAirports() = wrap {
        airportsService.getAirports().mapNotNull { it.toAirport() }
    }
}