package com.yomi.aeroporti.api.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AirportApiModel(
    val airportCode: String?,
    val internationalAirport: Boolean?,
    val domesticAirport: Boolean?,
    val airportName: String?,
    val city: City?,
    val country: Country?,
    val location: Location?
)

@JsonClass(generateAdapter = true)
data class City (
    val cityCode: String?,
    val cityName: String?,
    val timeZoneName: String?
)

@JsonClass(generateAdapter = true)
data class Country (
    val countryCode: String?,
    val countryName: String?
)

@JsonClass(generateAdapter = true)
data class Location(
    val latitude: Double?,
    val longitude: Double?
)



