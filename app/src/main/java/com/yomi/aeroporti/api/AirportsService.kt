package com.yomi.aeroporti.api

import com.yomi.aeroporti.api.model.AirportApiModel
import com.yomi.aeroporti.core.AeroportiConfig.Companion.AIRPORTS_ENDPOINT
import retrofit2.http.GET

interface AirportsService {

    @GET(AIRPORTS_ENDPOINT)
    suspend fun getAirports(): List<AirportApiModel>
}