package com.yomi.aeroporti.common

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Code from https://medium.com/androiddevelopers/livedata-with-snackbar-navigation-and-other-events-the-
 * singleliveevent-case-ac2622673150.
 *
 * This is a recommended practice by Google to handle single-live event (e.g., navigation, show toast/snack bar).
 */
open class UiEvent<out T>(private val content: T) {
    var hasBeenHandled = false
        private set

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

inline fun <T> LiveData<UiEvent<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline onEventUnhandledContent: (T) -> Unit
) {
    observe(owner, { event -> event?.getContentIfNotHandled()?.let(onEventUnhandledContent) })
}