package com.yomi.aeroporti.common

import android.app.Application
import com.yomi.aeroporti.BuildConfig
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        super.onCreate()
        NetworkUtil.init(this)
    }
}