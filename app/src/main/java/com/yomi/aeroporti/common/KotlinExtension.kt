package com.yomi.aeroporti.common

fun Any?.toStringOrEmpty(): String {
    return this?.run { toString() } ?: ""
}