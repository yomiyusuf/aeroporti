package com.yomi.aeroporti.common

import android.view.View
import android.widget.ViewFlipper
import java.lang.IllegalArgumentException

fun ViewFlipper.display(childView: View) {
    val childViewIndex = indexOfChild(childView)
    if (childViewIndex == -1) throw IllegalArgumentException("Child view is not found.")
    displayedChild = childViewIndex
}