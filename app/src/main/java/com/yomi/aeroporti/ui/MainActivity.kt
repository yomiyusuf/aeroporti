package com.yomi.aeroporti.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.yomi.aeroporti.R
import com.yomi.aeroporti.common.NetworkUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private var snackBar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        NetworkUtil.observe(this) { isConnected ->
            if (isConnected) {
                dismissError()
            } else {
                showError(getString(R.string.no_internet_msg))
            }
        }
    }

    private fun showError(errorMsg: String) {
        val root = findViewById<CoordinatorLayout>(R.id.root)
        snackBar = Snackbar.make(root, errorMsg , Snackbar.LENGTH_INDEFINITE)
        snackBar?.show()
    }

    private fun dismissError() {
        snackBar?.dismiss()
    }
}