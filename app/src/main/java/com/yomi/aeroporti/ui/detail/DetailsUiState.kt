package com.yomi.aeroporti.ui.detail

import com.yomi.aeroporti.ui.home.HomeAirportUiModel

sealed class DetailsUiState {
    data class Ready(val airport: HomeAirportUiModel) : DetailsUiState()
}