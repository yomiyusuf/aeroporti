package com.yomi.aeroporti.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yomi.aeroporti.R
import com.yomi.aeroporti.common.UiEvent
import com.yomi.aeroporti.data.AirportsRepository
import com.yomi.aeroporti.data.Result
import com.yomi.aeroporti.domain.Airport
import com.yomi.aeroporti.ui.home.HomeAirportUiModel
import com.yomi.aeroporti.ui.home.HomeUiEvents
import com.yomi.aeroporti.ui.home.HomeUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel : ViewModel() {

    private val _state = MutableLiveData<DetailsUiState>()
    val state: LiveData<DetailsUiState> = _state

    private val _events = MutableLiveData<UiEvent<DetailsUiEvents>>()
    val events: LiveData<UiEvent<DetailsUiEvents>> = _events

    fun initialize(airport: HomeAirportUiModel) {
        _state.value = DetailsUiState.Ready(airport)
    }

    fun onBackButtonClicked() {
        _events.value = UiEvent(DetailsUiEvents.ReturnToList)
    }
}