package com.yomi.aeroporti.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.yomi.aeroporti.R
import com.yomi.aeroporti.common.display
import com.yomi.aeroporti.common.observeEvent
import com.yomi.aeroporti.databinding.HomeFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModels()
    private var binding: HomeFragmentBinding? = null

    private val airportsAdapter = AirportsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = HomeFragmentBinding.inflate(inflater, container, false).also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        subscribeToUiState()
        subscribeToUiEvents()
    }

    private fun setupView() {
        binding?.airportsRv?.apply {
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL).also {
                it.setDrawable(ContextCompat.getDrawable(context, R.drawable.list_divider)!!)
            }
            addItemDecoration(decoration)
            adapter = airportsAdapter
        }
    }

    private fun subscribeToUiState() = binding?.run {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                HomeUiState.Loading -> container.display(loadingIndicator.root)
                is HomeUiState.Success -> {
                    container.display(content)
                    airportsAdapter.submitList(state.airports)
                    fab.setOnClickListener { airportsRv.scrollToPosition(0) } // smoothScrollToPosition() animation looks better but it's painful to watch when the list has been scrolled to far down
                }
                is HomeUiState.Error -> {
                    container.display(errorView.root)
                    errorView.apply {
                        errorMessage.text = getText(state.messageRes)
                        retryButton.setOnClickListener { state.retryAction?.invoke() }
                    }

                }
            }
        }
    }

    private fun subscribeToUiEvents() = viewModel.events.observeEvent(viewLifecycleOwner) { event ->
        when (event) {
            is HomeUiEvents.NavigateToDetail -> navigateToDetails(event.model)
        }
    }

    private fun navigateToDetails(model: HomeAirportUiModel) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(model)
        findNavController().navigate(action)
    }
}