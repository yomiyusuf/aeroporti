package com.yomi.aeroporti.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yomi.aeroporti.databinding.AirportItemBinding

class AirportsAdapter: ListAdapter<HomeAirportUiModel, AirportsAdapter.AirportViewHolder>(AirportItemDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirportViewHolder {
        val view = AirportItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AirportViewHolder(view)
    }

    override fun onBindViewHolder(holder: AirportViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class AirportViewHolder(private val binding: AirportItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(model: HomeAirportUiModel) {
            binding.apply {
                airportName.text = model.name
                countryName.text = model.country
                root.setOnClickListener { model.onClickAction?.invoke() }
            }

        }
    }
}

object AirportItemDiffCallback: DiffUtil.ItemCallback<HomeAirportUiModel>() {
    override fun areItemsTheSame(oldItem: HomeAirportUiModel, newItem: HomeAirportUiModel): Boolean {
        return oldItem.code == newItem.code
    }

    override fun areContentsTheSame(oldItem: HomeAirportUiModel, newItem: HomeAirportUiModel): Boolean {
        return oldItem == newItem
    }

}