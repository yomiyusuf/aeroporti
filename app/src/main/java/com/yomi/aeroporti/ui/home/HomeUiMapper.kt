package com.yomi.aeroporti.ui.home

import com.yomi.aeroporti.common.toStringOrEmpty
import com.yomi.aeroporti.domain.Airport

fun Airport.toHomeAirportUiModel(
    viewModel: HomeViewModel
): HomeAirportUiModel =
    HomeAirportUiModel(
        code = code,
        name = name,
        cityCode = cityCode,
        city = city,
        country = country,
        cityCountry = "$city, $country",
        lat = lat.toStringOrEmpty(),
        long = long.toStringOrEmpty(),
        timeZone = timezone.orEmpty(),
        isInternational = isInternational,
        isDomestic = isDomestic
    ).apply {
        onClickAction = { viewModel.onAirportClicked(this)}
    }