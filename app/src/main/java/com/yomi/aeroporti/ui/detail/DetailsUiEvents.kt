package com.yomi.aeroporti.ui.detail

import com.yomi.aeroporti.ui.home.HomeAirportUiModel

sealed class DetailsUiEvents {
    object ReturnToList: DetailsUiEvents()
}