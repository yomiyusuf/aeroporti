package com.yomi.aeroporti.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yomi.aeroporti.R
import com.yomi.aeroporti.common.UiEvent
import com.yomi.aeroporti.data.AirportsRepository
import com.yomi.aeroporti.data.Result
import com.yomi.aeroporti.domain.Airport
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: AirportsRepository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _state = MutableLiveData<HomeUiState>()
    val state: LiveData<HomeUiState> = _state

    private val _events = MutableLiveData<UiEvent<HomeUiEvents>>()
    val events: LiveData<UiEvent<HomeUiEvents>> = _events

    init {
        refresh()
    }

    private fun refresh() = viewModelScope.launch {
        _state.value = HomeUiState.Loading

        val response = withContext(dispatcher) {
            repository.getAllAirports()
        }

        handleResponse(response)
    }

    private fun handleResponse(response: Result<List<Airport>>) {
        when (response) {
            is Result.Ok -> {
                _state.value = HomeUiState.Success(response.value.map { it.toHomeAirportUiModel(this) })
            }
            is Result.Error -> {
                _state.value = HomeUiState.Error(R.string.generic_error_message).apply {
                    retryAction = { refresh() }
                }
            }
        }
    }

    fun onAirportClicked(airport: HomeAirportUiModel) {
        _events.value = UiEvent(HomeUiEvents.NavigateToDetail(airport))
    }
}