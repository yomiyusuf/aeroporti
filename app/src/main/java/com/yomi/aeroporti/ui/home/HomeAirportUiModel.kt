package com.yomi.aeroporti.ui.home

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class HomeAirportUiModel(
    val code: String,
    val name: String,
    val cityCode: String?,
    val city: String?,
    val country: String,
    val cityCountry: String,
    val timeZone: String,
    val lat: String,
    val long: String,
    val isInternational: Boolean,
    val isDomestic: Boolean
) : Parcelable {
    @IgnoredOnParcel
    var onClickAction: (() -> Unit)? = null
}