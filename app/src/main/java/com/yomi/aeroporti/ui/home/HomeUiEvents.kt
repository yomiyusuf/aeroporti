package com.yomi.aeroporti.ui.home

sealed class HomeUiEvents {
    data class NavigateToDetail(val model: HomeAirportUiModel): HomeUiEvents()
}