package com.yomi.aeroporti.ui.home

import androidx.annotation.StringRes

sealed class HomeUiState {
    object Loading : HomeUiState()
    data class Success(val airports: List<HomeAirportUiModel>) : HomeUiState()
    data class Error(@StringRes val messageRes: Int) : HomeUiState() {
        var retryAction: (() -> Unit)? = null
    }
}