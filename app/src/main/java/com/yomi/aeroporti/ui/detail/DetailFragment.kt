package com.yomi.aeroporti.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.yomi.aeroporti.R
import com.yomi.aeroporti.common.observeEvent
import com.yomi.aeroporti.databinding.DetailFragmentBinding
import com.yomi.aeroporti.ui.home.HomeAirportUiModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {
    private val viewModel: DetailsViewModel by viewModels()
    private var binding: DetailFragmentBinding? = null

    val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DetailFragmentBinding.inflate(inflater, container, false).also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initialize(args.airport)
        subscribeToUiState()
        subscribeToUiEvents()
        binding?.backButton?.setOnClickListener { viewModel.onBackButtonClicked() }
    }

    private fun subscribeToUiState() = binding?.run {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DetailsUiState.Ready -> setUpViews(state.airport)
            }
        }
    }

    private fun subscribeToUiEvents() = viewModel.events.observeEvent(viewLifecycleOwner) { event ->
        when (event) {
            DetailsUiEvents.ReturnToList -> findNavController().popBackStack(R.id.HomeFragment, false)
        }
    }

    private fun setUpViews(airport: HomeAirportUiModel) = binding?.run {
        with(airport) {
            airportNameView.text = name
            cityCountryView.text = cityCountry
            timezoneView.text = timeZone
            longitudeView.text = long
            latitudeView.text = lat
        }
    }
}