package com.yomi.aeroporti.domain

import com.yomi.aeroporti.api.model.AirportApiModel
import com.yomi.aeroporti.common.toStringOrEmpty

/**
 * Mapper to convert [AirportApiModel] to domain model [Airport]
 * For simplicity, we will assume that a valid airport should have at least a name and country.
 * If either of these is null, then it's is assumed the object is invalid and null is returned.
 */
fun AirportApiModel.toAirport(): Airport? {

    return if (airportName != null && country?.countryName != null)
        Airport(
            code = airportCode.orEmpty(),
            name = airportName,
            cityCode = city?.cityCode.orEmpty(),
            city = city?.cityName.orEmpty(),
            country = country.countryName,
            lat = location?.latitude,
            long = location?.longitude,
            timezone = city?.timeZoneName,
            isInternational = internationalAirport?: false, // default to false for unavailable value
            isDomestic = domesticAirport?: false // default to false for unavailable value
        )
    else
        null
}