package com.yomi.aeroporti.domain

data class Airport(
    val code: String,
    val name: String,
    val cityCode: String,
    val city: String,
    val country: String,
    val lat: Double?,
    val long: Double?,
    val timezone: String?,
    val isInternational: Boolean,
    val isDomestic: Boolean
)