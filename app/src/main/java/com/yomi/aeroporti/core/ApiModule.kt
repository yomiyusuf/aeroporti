package com.yomi.aeroporti.core

import com.squareup.moshi.Moshi
import com.yomi.aeroporti.api.AirportsService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    @Provides
    @Singleton
    fun provideRetrofit(
        config: AeroportiConfig,
        moshi: Moshi,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(config.baseUrl())
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    @Provides
    @Singleton
    fun provideMoviesClient(retrofit: Retrofit) = retrofit.create(AirportsService::class.java)
}