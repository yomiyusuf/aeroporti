package com.yomi.aeroporti.core

class AeroportiConfig{
    fun baseUrl() = "https://api.qantas.com/flight/refData/"

    companion object {
        const val AIRPORTS_ENDPOINT = "airport"
    }
}